class mysql {

	$service = 'mysqld'
	$services = [ 'mysql', 'mysql-server', 'python3-PyMySQL' ]

	package { $services:
		ensure => present,
	}
	
	service { $service:
                ensure => running,
                enable => true,
                subscribe => Package[$services],
        }
}
