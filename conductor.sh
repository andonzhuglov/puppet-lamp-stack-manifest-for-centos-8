#!/bin/bash

# Colors
purple=$(tput setaf 141);
lpurple=$(tput setaf 105);
yellow=$(tput setaf 228);
bold=$(tput bold);
re=$(tput sgr0);
BASE_PATH="/etc/puppetlabs/code/environments/production/"

announce() { echo -e "\n${bold}${yellow}$1${re} \n" && sleep 0.5 ; }

checking() { echo "$?" ; }

function conduct {
	services=('apache' 'mysql' 'php')
	modules_path="${BASE_PATH}modules/"
	manifests_path="${BASE_PATH}manifests/"
	read -n 1 -p "${bold}${lpurple}Distribute modules and manifests ? [y/n] ${re}" answer
	echo
	if [[ "$answer" == "y" ]];
	then
		for service in "${services[@]}"
		do
			mv $service/ $modules_path/$service 2>>error_log.txt
		       	if [[ checking != "0" ]];
			then
				echo -e "${bold}${yellow}$service${re} was not moved. Folder does not exist in the current directory (refer to error_log.txt)"
			else
				echo -e "${bold}${yellow}$service${re} was moved to the Puppet Modules folder ${bold}${purple}$modules_path${re}"
			fi
		done
	else
		echo -e "${bold}The Modules were not moved. You can move them manually in ${purple}${BASE_PATH}modules${re}"	
	fi
	echo

	read -n 1 -p "${bold}${lpurple}Remove existing manifests ${yellow}site.pp${re} ${bold}${lpurple}and substitude with the remote repository one? [y/n] ${re}" answer_2
	echo
	if [[ "$answer_2" == "y" ]];
	then
		mv site.pp $manifests_path/site.pp 2>>error_log.txt
                if [[ checking != "0" ]];
                then
                        echo -e "${bold}${yellow}site.pp${re} ${bold}was not moved to. Refer to error_log.txt"
                else
                        echo -e "${bold}${yellow}site.pp${re} ${bold}was moved to ${purple}${manifests_path}${re}"
                fi
        else
                echo -e "${bold}${yellow}site.pp${re} ${bold}was not moved to ${purple}${manifests_path}${re}. You can add the details manually"
        fi
        echo
	
}

function domain_changer {
	paths=("${BASE_PATH}modules/apache/manifests/init.pp" "${BASE_PATH}manifests/site.pp")
	read -n 1 -p "${bold}${lpurple}Would you like to assign your domain to the Virtual Host and set up html and logs folders automatically? [y/n] ${re}" answer_3
       	echo
	if [[ "$answer_3" == "y" ]];
	then
		read -p "${bold}${lpurple}Which domain would you like to assign? (format: example.com):  ${re}" domain_name
		for path in "${paths[@]}"
		do
			sed -i "s/example.com/$domain_name/g" "$path"
			echo -e "${bold}${yellow}$domain_name${re} has been assigned to the Manifest in ${bold}${purple}$path${re}"	
		done
	else
		echo -e "${bold}You can manually assign your domain in the configuration files under ${bold}${purple}${paths[0]}${re} and ${bold}${purple}${paths[1]}${re}"
	fi
}

function main {
	announce "Conductor will destribute the Modules and Manifests for you"
	conduct
	domain_changer
	announce "Program is closing"
}
main
