
# Puppet Modules for LAMP Stack

The current Puppet project will help you install LAMP Stack (Linux, Apache, MySQL and PHP) on a CentOS 9 server. It will also create a custom index.php file and place it in your domain html directory.

## Structure of the modules

```shell
|-- apache
|   |-- files
|   |   `-- index.php
|   |-- manifests
|   |   `-- init.pp
|   `-- templates
|       `-- vhost.conf.erb
|-- conductor.sh
|-- mysql
|   `-- manifests
|       `-- init.pp
|-- php
|   `-- manifests
|       `-- init.pp
`-- site.pp

8 directories, 7 files
```

What are the files used for?

- `apache/files/index.pp` is a Template file containing the code for our custom index page.
- `apache/manifests/init.pp` Contains our Apache Manifest file.
- `apache/templates/vhost.conf.erb` is a Template file that will configure our Virtual Host.
- `conductor.sh` is a script that will move the modules and manifests to their respective puppet directories and adjust the domain for the Virtual Host.
- `mysql/manifests/init.pp` is the Manifest file for MySQL.
- `php/manifests/init.pp` like the above, this init.pp holds the Manifest instructions for the PHP Module.
- `site.pp` our Manifest file for nodes and tasks distribution.

## Environment setup

For this project, we will use a Puppet Master server that will provide instructions towards two independent Puppet Agents ( 1 and 2 ). Puppet Agent 1 will install Apache and PHP and Puppet Agent 2 will take care of MySQL. Both Agents will ensure that services are enabled and started. The Agent servers should contain the Puppet Master IP in their `/etc/hosts/` which will ensure that the SSL connection can be established correctly.

![Diagram](https://snipboard.io/yMtLKV.jpg)

Next we need to configure the Puppet Master manifest to build our servers. The file in question is located in `/etc/puppetlabs/code/environments/production/manifests/site.pp` and contains the following:

```pp
node "puppet-agent" {
	$domain_name = 'example.com'

	include apache
	include php
}

node "puppet-agent2" {
	include mysql
}
```
where we should change the value of `$domain_name` from `example.com` to the desired domain name for our website. The domain name will be also used to configure our Virtual Host file:

```erb
<VirtualHost *:80>
    ServerName <%= @domain_name %>
    ServerAlias www.<%= @domain_name %>
    DocumentRoot /var/www/<%= @domain_name -%>/html/
    ErrorLog /var/log/<%= @domain_name -%>_error.log
    CustomLog /var/log/<%= @domain_name -%>_access.log combined

   <Directory /var/www/<%= @domain_name %>>
         Options -Indexes
   </Directory>

   <IfModule mod_dir.c>
       DirectoryIndex index.php index.html index.cgi index.pl  index.xhtml index.htm
   </IfModule>
</VirtualHost>
```


We will take the same action for our Apache Module's Manifest file located in: `/etc/puppetlabs/code/environments/production/modules/apache/manifests/init.pp` and modify the variable's value `$domain_name = 'example.com'` to the domain we plan to use.

Both domains can be automatically configured in the script ```conductor.sh``` that is included in this repository. For reference see the step 1 below.

## Running Puppet 

### 1. Clone the repo in your Puppet Master server

Using the following command:

```command
git clone https://gitlab.com/andonzhuglov/puppet-lamp-stack-manifest-for-centos-8.git
cd puppet-lamp-stack-manifest-for-centos-8
ls
```

The files we should see are:
```shell
apache  conductor.sh  mysql  php  README.md
```

### 2. Execute the ```conductor.sh``` script on the Puppet Master server

We can start our script with:

```command
bash conductor.sh
```

This script will help us distribute the modules and manifests to their respective puppet directories and prompt if we want to modify the ```$domain_name```'s value for our ```site.pp``` and Apache's ```init.pp``` files:

![Terminal Output](https://snipboard.io/FeoSf2.jpg)



### 3. Install the LAMP Stack

The installation for both web and database destined servers will take place as soon as we run the below mentioned command on our Puppet Agent 1 & 2 servers:
```command
puppet agent -t
```
Puppet Agent 1 will have a running and enabled Apache + PHP and Puppet Agent 2 will run MySQL.



The above configuration was tested using CentOS 9 servers in DigiOcean.com obtaining positive results. Any feedback regarding the above would be appreicated. You can contact me at azhuglov@gmail.com.
