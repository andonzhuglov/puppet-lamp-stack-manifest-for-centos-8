class apache {
	
	$domain_name = 'example.com'
	$service_name = 'httpd'
	$domain_folder = "/var/www/${domain_name}"
	$public_html = "/var/www/${domain_name}/html"
	$virtual_host = "/etc/httpd/conf.d/${domain_name}.conf"
	$custom_index = "/var/www/${domain_name}/html/index.php"

	package { $service_name:
		ensure => present,
	}
	
	file { $domain_folder:
		ensure => directory,
		owner => 'root',
		mode => '0755',
	}

	file { $public_html:
                ensure => directory,
                owner => 'root',
                mode => '0755',
        }

	file { $virtual_host:
		ensure => file,
		owner => 'root',
		mode => '0755',
		content => template('apache/vhost.conf.erb'),
		require => Package[$service_name],
	}

	service { $service_name:
		ensure => running,
		enable => true,
		subscribe => File[$virtual_host],
	}

	file { $custom_index:
		ensure => file,
		owner => 'root',
		mode => '0755',
		source => 'puppet:///modules/apache/index.php',
		subscribe => Package[$service_name],
	}
}

